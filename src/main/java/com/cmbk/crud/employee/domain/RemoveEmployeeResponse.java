package com.cmbk.crud.employee.domain;

/**
 * @author chanaka.k
 *
 */
public class RemoveEmployeeResponse {

	private Status status;

	public RemoveEmployeeResponse() {
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

}
