package com.cmbk.spring.doc;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
class EmployeeServiceCrudBlogApplicationTests {

	@Test
	void contextLoads() {
	}

}
