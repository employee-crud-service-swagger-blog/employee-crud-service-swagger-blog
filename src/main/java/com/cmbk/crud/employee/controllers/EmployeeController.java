package com.cmbk.crud.employee.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cmbk.crud.employee.controllers.common.HTTPResponseHandler;
import com.cmbk.crud.employee.controllers.common.RequestMappings;
import com.cmbk.crud.employee.domain.AllEmployeeResponse;
import com.cmbk.crud.employee.domain.CreateEmployeeRequest;
import com.cmbk.crud.employee.domain.CreateEmployeeResponse;
import com.cmbk.crud.employee.domain.EmployeeResponse;
import com.cmbk.crud.employee.domain.EmployeeValidator;
import com.cmbk.crud.employee.domain.RemoveEmployeeResponse;
import com.cmbk.crud.employee.domain.UpdateEmployeeRequest;
import com.cmbk.crud.employee.services.EmployeeService;

/**
 * @author chanaka.k
 *
 */
@RestController
@RequestMapping(RequestMappings.EMPLOYEES)
public class EmployeeController extends HTTPResponseHandler {

	private final String EMPLOYEE_ENDPOINTS_RUNNING = "Employee CRUD endpoints are running";

	final static Logger logger = Logger.getLogger(EmployeeController.class);

	@Autowired
	private EmployeeService employeeService;

	@RequestMapping(value = RequestMappings.CONTEXT_PATH, method = RequestMethod.GET)
	public String plainRequest() {
		return EMPLOYEE_ENDPOINTS_RUNNING;
	}

	/**
	 * Create new employee
	 *
	 */
	@RequestMapping(value = RequestMappings.CREATE_EMPLOYEE, method = RequestMethod.POST)
	public @ResponseBody CreateEmployeeResponse createEmployee(@RequestBody CreateEmployeeRequest request) {

		EmployeeValidator.validateCreateEmployeeRequest(request);

		logger.info("Inside > create new employee endpoint ");

		return employeeService.createEmployee(request);

	}

	/**
	 * Retrieve employee by employee id
	 *
	 */
	@RequestMapping(value = RequestMappings.RETRIVE_EMPLOYEE_BY_ID, method = RequestMethod.GET)
	public @ResponseBody EmployeeResponse retriveEmployeeById(
			@PathVariable(value = "employee-id", required = true) String employeeId) {

		logger.info("Inside > retrieve employee by id endpoint ");

		return employeeService.retrieveEmployeeById(employeeId);

	}

	/**
	 * Delete employee by employee id
	 *
	 */
	@RequestMapping(value = RequestMappings.REMOVE_EMPLOYEE_BY_ID, method = RequestMethod.DELETE)
	public @ResponseBody RemoveEmployeeResponse removeEmployeeById(
			@PathVariable(value = "employee-id", required = true) String employeeId) {

		logger.info("Inside > remove employee by id endpoint ");

		return employeeService.removeEmployeeById(employeeId);

	}

	/**
	 * Update employee
	 *
	 */
	@RequestMapping(value = RequestMappings.UPDATE_EMPLOYEE_BY_ID, method = RequestMethod.PUT)
	public @ResponseBody EmployeeResponse updateEmployeeById(@RequestBody UpdateEmployeeRequest request,
			@PathVariable(value = "employee-id", required = true) String employeeId) {

		logger.info("Inside > update employee by id endpoint ");

		return employeeService.updateEmployee(employeeId, request);

	}

	/**
	 * Retrieve all employees
	 *
	 */
	@RequestMapping(value = RequestMappings.RETRIVE_ALL_EMPLOYEES, method = RequestMethod.GET)
	public @ResponseBody AllEmployeeResponse retriveAllEmployees() {

		logger.info("Inside > retrieve all employees endpoint ");

		return employeeService.retrieveAllEmployees();

	}
}
