package com.cmbk.crud.employee.domain.transform;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.cmbk.crud.employee.domain.AllEmployeeResponse;
import com.cmbk.crud.employee.domain.CreateEmployeeRequest;
import com.cmbk.crud.employee.domain.CreateEmployeeResponse;
import com.cmbk.crud.employee.domain.Employee;
import com.cmbk.crud.employee.domain.EmployeeResponse;

/**
 * @author chanaka.k
 *
 */
public class EmployeeTransformer {

	public static Employee transformEmployeeRequestToDto(CreateEmployeeRequest createEmployeeRequest) {

		Employee employee = new Employee();
		employee.setFirstName(createEmployeeRequest.getFirstName());
		employee.setJoinedDate(createEmployeeRequest.getJoinedDate());
		employee.setLastName(createEmployeeRequest.getLastName());
		employee.setSallary(createEmployeeRequest.getSallary());
		employee.setId(UUID.randomUUID().toString());

		return employee;
	}

	public static CreateEmployeeResponse transformEmployeeDtoToResponse(Employee employee) {

		CreateEmployeeResponse response = new CreateEmployeeResponse();
		response.setFirstName(employee.getFirstName());
		response.setJoinedDate(employee.getJoinedDate());
		response.setLastName(employee.getLastName());
		response.setSallary(employee.getSallary());

		return response;
	}

	public static EmployeeResponse employeeToEmployeeResponse(Employee employee) {

		EmployeeResponse employeeResponse = new EmployeeResponse();
		employeeResponse.setFirstName(employee.getFirstName());
		employeeResponse.setJoinedDate(employee.getJoinedDate());
		employeeResponse.setLastName(employee.getLastName());
		employeeResponse.setSallary(employee.getSallary());

		return employeeResponse;
	}

	public static AllEmployeeResponse employeesToAllEmployeeResponse(List<Employee> employees) {

		AllEmployeeResponse allEmployeeResponse = new AllEmployeeResponse();
		List<EmployeeResponse> employeeResponses = new ArrayList<EmployeeResponse>();

		for (Employee employee : employees) {

			EmployeeResponse response = new EmployeeResponse();
			response.setFirstName(employee.getFirstName());
			response.setJoinedDate(employee.getJoinedDate());
			response.setLastName(employee.getLastName());
			response.setSallary(employee.getSallary());
			employeeResponses.add(response);
		}

		allEmployeeResponse.setEmployees(employeeResponses);

		return allEmployeeResponse;
	}

}
