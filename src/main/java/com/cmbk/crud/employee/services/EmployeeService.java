package com.cmbk.crud.employee.services;

import com.cmbk.crud.employee.domain.AllEmployeeResponse;
import com.cmbk.crud.employee.domain.CreateEmployeeRequest;
import com.cmbk.crud.employee.domain.CreateEmployeeResponse;
import com.cmbk.crud.employee.domain.EmployeeResponse;
import com.cmbk.crud.employee.domain.RemoveEmployeeResponse;
import com.cmbk.crud.employee.domain.UpdateEmployeeRequest;

/**
 * @author chanaka.k
 *
 */
public interface EmployeeService {

	public AllEmployeeResponse retrieveAllEmployees();

	public EmployeeResponse retrieveEmployeeById(String employeeID);

	public CreateEmployeeResponse createEmployee(CreateEmployeeRequest createEmployeeRequest);

	public RemoveEmployeeResponse removeEmployeeById(String employeeId);

	public EmployeeResponse updateEmployee(String employeeId, UpdateEmployeeRequest request);

}
