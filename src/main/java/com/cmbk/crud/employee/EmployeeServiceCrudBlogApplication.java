package com.cmbk.crud.employee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author chanaka.k
 *
 */
@SpringBootApplication(scanBasePackages = { "com.cmbk.crud.employee" })
public class EmployeeServiceCrudBlogApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeServiceCrudBlogApplication.class, args);
	}

}
