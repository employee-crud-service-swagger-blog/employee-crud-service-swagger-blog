package com.cmbk.crud.employee.services;

import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.HttpServerErrorException;

import com.cmbk.crud.employee.domain.AllEmployeeResponse;
import com.cmbk.crud.employee.domain.CreateEmployeeRequest;
import com.cmbk.crud.employee.domain.CreateEmployeeResponse;
import com.cmbk.crud.employee.domain.Employee;
import com.cmbk.crud.employee.domain.EmployeeResponse;
import com.cmbk.crud.employee.domain.RemoveEmployeeResponse;
import com.cmbk.crud.employee.domain.Status;
import com.cmbk.crud.employee.domain.UpdateEmployeeRequest;
import com.cmbk.crud.employee.domain.common.exception.NotFoundException;
import com.cmbk.crud.employee.domain.transform.EmployeeTransformer;
import com.cmbk.crud.employee.repositories.EmployeeRepository;

/**
 * @author chanaka.k
 *
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {

	private final static String DELETE_MSG_SUCCESS = "Successfully deleted";

	private final static String DELETE_MSG_UNSUCCESS = "Error while deleting";

	final static Logger logger = Logger.getLogger(EmployeeServiceImpl.class);

	@Autowired
	private EmployeeRepository employeeRepository;

	@Override
	public CreateEmployeeResponse createEmployee(CreateEmployeeRequest createEmployeeRequest) {

		logger.info("Inside create employee service");

		Employee employee = null;
		CreateEmployeeResponse response = null;

		try {

			employee = EmployeeTransformer.transformEmployeeRequestToDto(createEmployeeRequest);
			employee = employeeRepository.save(employee);
			response = EmployeeTransformer.transformEmployeeDtoToResponse(employee);

		} catch (HttpServerErrorException hse) {
			logger.error("Server Error found : " + hse.getMessage().toString());
			hse.printStackTrace();
		} catch (Exception e) {
			logger.error("Error found : " + e.getMessage().toString());
			e.printStackTrace();
		}

		logger.info("Successfully saved new employee");

		return response;
	}

	@Override
	public AllEmployeeResponse retrieveAllEmployees() {

		logger.info("Inside retrieve all employee details, service");

		List<Employee> employees = null;
		AllEmployeeResponse allEmployeeResponse = null;

		try {

			employees = employeeRepository.findAll();
			allEmployeeResponse = EmployeeTransformer.employeesToAllEmployeeResponse(employees);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new NotFoundException("EMPLOYEES_NOT_FOUND", "Error while loading all employ details");
		}

		logger.info("Successfully retrieved all employee details");
		return allEmployeeResponse;
	}

	@Override
	public RemoveEmployeeResponse removeEmployeeById(String employeeId) {

		logger.info("Inside remove employee by id, service");

		RemoveEmployeeResponse removeEmployeeResponse = null;
		Optional<Employee> optionalEmployee = null;

		optionalEmployee = employeeRepository.findById(employeeId);

		if (!optionalEmployee.isPresent()) {
			throw new NotFoundException("INVALID_EMPLOYEE_ID", "Employee not found for the given id");
		}

		try {

			employeeRepository.deleteById(employeeId);
			removeEmployeeResponse = new RemoveEmployeeResponse();
			Status status = new Status();
			status.setStatusCode("SUCCESS");
			status.setStatusDescription(DELETE_MSG_SUCCESS);
			removeEmployeeResponse.setStatus(status);

		} catch (Exception e) {

			removeEmployeeResponse = new RemoveEmployeeResponse();
			Status status = new Status();
			status.setStatusCode("FAILED");
			status.setStatusDescription(DELETE_MSG_UNSUCCESS);
			removeEmployeeResponse.setStatus(status);
		}
		logger.info("Successfully removed employee");
		return removeEmployeeResponse;
	}

	@Override
	public EmployeeResponse retrieveEmployeeById(String employeeId) {

		logger.info("Inside retrieve employee by id, service");

		Optional<Employee> optionalEmployee = null;
		Employee employee = null;
		EmployeeResponse employeeResponse = null;

		optionalEmployee = employeeRepository.findById(employeeId);

		if (optionalEmployee.isPresent()) {
			employee = optionalEmployee.get();
		} else {
			throw new NotFoundException("INVALID_EMPLOYEE_ID", "Employee not found for the given id");
		}

		employeeResponse = EmployeeTransformer.employeeToEmployeeResponse(employee);

		logger.info("Successfully returned employee by id");
		return employeeResponse;
	}

	@Override
	public EmployeeResponse updateEmployee(String employeeId, UpdateEmployeeRequest request) {

		logger.info("Inside update employee by id, service");

		Employee exsistingEmployee = null;
		EmployeeResponse employeeResponse = null;

		if (!employeeRepository.findById(employeeId).isPresent()) {
			throw new NotFoundException("INVALID_EMPLOYEE_ID", "Employee not found for the given id");
		}

		exsistingEmployee = employeeRepository.findById(employeeId).get();

		if (StringUtils.hasText(request.getFirstName())) {
			exsistingEmployee.setFirstName(request.getFirstName());
		}
		if (request.getJoinedDate() != null) {
			exsistingEmployee.setJoinedDate(request.getJoinedDate());
		}
		if (StringUtils.hasText(request.getLastName())) {
			exsistingEmployee.setLastName(request.getLastName());
		}
		if (request.getSallary() != null) {
			exsistingEmployee.setSallary(request.getSallary());
		}

		employeeRepository.save(exsistingEmployee);

		employeeResponse = EmployeeTransformer.employeeToEmployeeResponse(exsistingEmployee);

		logger.info("Successfully updated employee object");

		return employeeResponse;
	}

}
