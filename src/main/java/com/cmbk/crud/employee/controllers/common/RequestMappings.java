package com.cmbk.crud.employee.controllers.common;

/**
 * @author chanaka.k
 *
 */
public interface RequestMappings {
	
	public static String EMPLOYEES = "employees";
	
	public static String CONTEXT_PATH = "/";
	
	public static String CREATE_EMPLOYEE = "/add";
	
	public static String RETRIVE_EMPLOYEE_BY_ID = "/{employee-id}";
	
	public static String REMOVE_EMPLOYEE_BY_ID = "/{employee-id}";
	
	public static String UPDATE_EMPLOYEE_BY_ID = "/{employee-id}";
	
	public static String RETRIVE_ALL_EMPLOYEES = "/all";
	
}
