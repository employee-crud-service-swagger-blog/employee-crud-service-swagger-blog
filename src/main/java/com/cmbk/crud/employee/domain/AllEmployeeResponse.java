package com.cmbk.crud.employee.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * @author chanaka.k
 *
 */
public class AllEmployeeResponse {

	private List<EmployeeResponse> employees;

	public AllEmployeeResponse() {
	}

	public List<EmployeeResponse> getEmployees() {
		if(employees == null) {
			employees = new ArrayList<EmployeeResponse>();
		}
		return employees;
	}

	public void setEmployees(List<EmployeeResponse> employees) {
		this.employees = employees;
	}

}
